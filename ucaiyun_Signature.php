
<?php
@ini_set('display_errors', '0');
error_reporting(0);
@set_time_limit(300);
@ignore_user_abort(true);

define('IN_CAIJIAPI',true);
	
class CaiJiApi{
	var $apiname='bcac9915f16a5079d7a44d8c791380e8';
	var $apipass='528e40b0beea937db2c5a0a893068a86';

	function __construct(){		
		if(!$this->checkSignature($_SERVER['HTTP_HOST'],'/ucaiyun_Signature.php',$_GET['ACTION'],$_SERVER['HTTP_HOST'],$this->apiname,$this->apipass,$_GET['NONCE'],$_GET['TIMESTAMP'],$_GET['Signature'])){
			exit("Sign Failed");
		}
	}
    function CaiJiApi(){
		$this->__construct();
    }
	function getSignedUrl($server,$path,$action,$domain,$apiname,$apipass){
		$NONCE=mt_rand(1,65535);
		$TIMESTAMP=time();
		$SIGNSTRING="server=$server&path=$path&ACTION=$action&DOMAIN=$domain&APINAME=$apiname&NONCE=$NONCE&TIMESTAMP=$TIMESTAMP";
		$Signature = base64_encode(hash_hmac('sha1', $SIGNSTRING, $apipass, true));
		$Signature=urlencode($Signature);
		$url="http://$server"."$path?ACTION=$action&DOMAIN=$domain&APINAME=$apiname&NONCE=$NONCE&TIMESTAMP=$TIMESTAMP&Signature=$Signature";
		return $url;
	}
	function checkSignature($server,$path,$action,$domain,$apiname,$apipass,$NONCE,$TIMESTAMP,$Signature){
		if(time()-$TIMESTAMP>36000) return false;
		$SIGNSTRING="server=$server&path=$path&ACTION=$action&DOMAIN=$domain&APINAME=$apiname&NONCE=$NONCE&TIMESTAMP=$TIMESTAMP";
		return base64_encode(hash_hmac('sha1', $SIGNSTRING, $apipass, true)) === $Signature;
	}
	function encoding_converter($string, $from_encoding, $target_encoding){
		if (function_exists('mb_convert_encoding')){
			return mb_convert_encoding($string, str_replace('//IGNORE', '', strtoupper($target_encoding)), $from_encoding);
		}
		elseif (function_exists('iconv')){
			if (strtoupper($from_encoding) == 'UTF-16'){
				$from_encoding = 'UTF-16BE';
			}
			if (strtoupper($target_encoding) == 'UTF-16'){
				$target_encoding = 'UTF-16BE';
			}
			if (strtoupper($target_encoding) == 'GB2312' or strtoupper($target_encoding) == 'GBK'){
				$target_encoding .= '//IGNORE';
			}
			return iconv($from_encoding, $target_encoding, $string);
		}
		else{
			return $string;
		}
	}
	function charSetAdapter($content,$toCharset,$fromCharset){
		if(is_array($content)){
			foreach($content as $key => $val){
				$content[$key] = $this->charSetAdapter($val,$toCharset,$fromCharset);
			}
			return $content;
		}
		else{
			return $this->encoding_converter($content,$fromCharset,$toCharset);
		}
	}
	function Savearticle_action(){
		if(!file_exists(dirname(__FILE__).'/ucaiyun_Savearticle.php')){
			exit("发布接口未安装");
		}
		$_POST['returnArticle']= get_magic_quotes_gpc()?stripslashes($_POST['returnArticle']):$_POST['returnArticle'];
		$return=json_decode(base64_decode($_POST['returnArticle']),1);
		if(!$return){
			$return=json_decode($_POST['returnArticle'],1);
		}
		$_POST=null;
		return $return;
	}
	function get_contents_action(){
		if(!file_exists(dirname(__FILE__).'/ucaiyun_Savearticle.php')){
			echo "发布接口未安装";
		}
		else if(isset($_GET['url']) && stripos($_GET['url'],'://')!==false){
			$url=urldecode($_GET['url']);
			$start=microtime(1);
			$html = $this->curl_get_contents($url,20,trim($_GET['ua']));
			echo base64_encode(json_encode(array('content'=>$html,'timeout'=>microtime(1)-$start)));
		}
	}
	function curl_get_contents($url, $timeout = 5,$ua='baidu-pc'){	
		if (function_exists('curl_init')){
			$UAs=array(
				'baidu-m'   =>'Mozilla/5.0 (Linux;u;Android 4.2.2;zh-cn;) AppleWebKit/534.46 (KHTML,like Gecko) Version/5.1 Mobile Safari/10600.6.3 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)',
				'baidu-pc'  =>'Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)',
				'else-m'    =>'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.23 Mobile Safari/537.36',
				'else-pc'   =>'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
				'weixin'    =>'Mozilla/5.0 (Linux; Android 7.1.1; MI 6 Build/NMF26X; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.132 MQQBrowser/6.2 TBS/043807 Mobile Safari/537.36 MicroMessenger/6.6.1.1220(0x26060135) NetType/4G Language/zh_CN',
				'no-ua'     =>''
			);
			$request_header=array(
				'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
				'Accept-Language:zh-CN,zh;q=0.9',
				'Upgrade-Insecure-Requests:1',
				'Expect:'
			);
			if (!function_exists('curl_init')){
				return false;
			}
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			
			$parseUrl = parse_url($url);
			curl_setopt($curl, CURLOPT_REFERER, $parseUrl['scheme']."://".$parseUrl['host'].'/');

			curl_setopt($curl, CURLOPT_HTTPHEADER, $request_header);
			//curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
			curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($curl, CURLOPT_HEADER, false);
			if (ini_get('open_basedir') == '' && ini_get('safe_mode' == 'Off')) {
				curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			}
			curl_setopt($curl, CURLOPT_USERAGENT, $UAs[$ua]);
			curl_setopt($curl, CURLOPT_COOKIE, '');
			curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
			curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
			$result = curl_exec($curl);

			if (curl_errno($curl) === 23 || curl_errno($curl) === 61) {
				curl_setopt($curl, CURLOPT_ENCODING, 'none');
				$result = curl_exec($curl);
			}
			$this->last_curl_location = curl_getinfo($curl,CURLINFO_EFFECTIVE_URL);
			$this->httpCode=intval(curl_getinfo($curl,CURLINFO_HTTP_CODE));
			$this->last_curl_error=curl_error($curl);
			
			curl_close($curl);
			return trim($result);
		}
		elseif(ini_get('allow_url_fopen')){
			return @file_get_contents($url, false, stream_context_create(array('http' => array('timeout' => $timeout)))); 
		}
		else return false;
	}
}
$action=isset($_GET['ACTION'])?$_GET['ACTION']:'';
$action_method = $action . '_action';

$CaiJiApi=new CaiJiApi($action);
if (! is_object($CaiJiApi) || ! method_exists($CaiJiApi, $action_method)){
	header('HTTP/1.1 404 Not Found');
	header("status: 404 Not Found");
	exit;
}

$return = $CaiJiApi->$action_method();
if($action=='Savearticle' && isset($return['rsm'])){
	$article = $return['rsm'];
	$return = null;
	ob_start();
	$html= "Article received, title:<br>{$article['arc_title']}<br>Saving...";
	echo $html;

	$article=$CaiJiApi->charSetAdapter($article,$article['mycharset'],$article['fromcharset']);
	include(dirname(__FILE__).'/ucaiyun_Savearticle.php');
	$html2=ob_get_contents();
	ob_end_clean();
	echo strpos($html2,$html)===false?($html.$html2):$html2;
}
?>
