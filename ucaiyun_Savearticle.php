
<?php
if (!defined('IN_CAIJIAPI')) exit;
@ini_set('display_errors', '0');
error_reporting(0);
@set_time_limit(300);
@ignore_user_abort(true);

define('APPTYPEID', 2);
define('CURSCRIPT', 'forum');
define('JK_CHARSET','utf-8');
define('NOROBOT', true);

$SaveArticle = new SaveArticle($article);
$SaveArticle->run();

class SaveArticle{
	var $article;
	var $replys = array();
	var $config = array(
		'使用签名'			=>			0,							//使用签名（1或0）
		'注册会员邮件后缀'	=>			'126.com|||163.com',		//多个用|||分割
		'数据库用户'		=>			1,							//是否从数据随机取出用户，如果为1从数据库随机取出数据。
		'回复用户名'		=>			'回复用户1|||回复用户2|||回复用户3|||回复用户4|||回复用户5|||回复用户6',	//回复用户名，把想充当回复的用户名放到这里，用|||分割，用户名不需要在论坛已经存在，如果没有会自动注册
		'用户密码'			=>			'',							//新注册用户的密码			
		'用户密码参数'		=>			'jksdjfs',					//用户密码参数，建议随意修改
		'回复间隔'			=>			array( 30 , 60 ),			//回复间隔,单位是秒。会随机在2个数字的中间取值
		'回复用户签名'		=>			'',							//回复用户签名,为空新注册的用户没有签名，否则设置签名，随机签名
		'分隔符'			=>			'|||',						//采集器标签循环的分隔符
		'附件权限组'		=>			'',							//权限多个用|分割
		'附件价格'			=>			'1|2|3|4|6',				//价格多个用|分割
		'附件隐藏'			=>			0,
		'fname'				=>			array('板块1'=>'1','板块2'=>'2'),//用来映射fid的值的
		'typename'			=>			array('站长杂谈'=>'1','分类2'=>'2'),//用来映射typeid的值的
		'新注册用户群组'	=>			array(),					//用户组的id写里面，新注册的用户就是这个用户组了。可以写多个。如：array(16,17,18);		
	);
	function SaveArticle($arc){
		$this->__construct($arc);
	}
	
	function __construct($arc){
		require './source/class/class_core.php';
		require './source/function/function_forum.php';
		require_once libfile('function/misc');
		require_once libfile('function/member');
		require_once libfile('function/editor');
		$_POST=null;
		C::app()->init();
		$this->article = $arc;
	}
	
	function run(){
		$this->prepare();
		$this->checkAndInitData();
		runhooks();
		set_rssauth();
		loadforum();
		loaducenter();
		$this->logonUser();
		$this->newthread();
		$this->reply();
		$this->baiduPush($this->titleurl,$this->article['baidutoken']);
		echo 'Article Saved!';
	}
	
	function prepare(){
		$_POST['subject']=$this->article['arc_title'];
		$_POST['publishdate']=date('Y-m-d H:i:s',time());
		$_POST['fid']=$this->article['typeid'];
		$_POST['tags']=$this->article['arc_keywords'];
		
		$this->article['arc_body']=$this->splitPage($this->article['arc_body']);
		$this->article['arc_body']=$this->setInnerLink($this->article['arc_body']);
		$this->article['arc_body']=$this->getPicmessage($this->article['download_pic'],$this->article['arc_body']);
		$this->article['arc_body']=$this->setStyle($this->article['arc_body']);
		$_POST['message']=$this->html2bbcode($this->article['arc_body']);
		
		$max = C::t('common_member')->max_uid();
		$randuid = rand(1, $max);
		$user_info = DB::fetch_first("SELECT uid,username FROM ".DB::table('common_member')." where uid >= $randuid order by uid ASC LIMIT 1" );
		$_POST['username'] = $user_info['username'];
		$this->uid=$user_info['uid'];
		$_GET=$_POST;
	}
	
	function setStyle($body){
		if(isset($this->article['style']['p'])){
			$body=str_replace('<p>',"<p style='".$this->article['style']['p']."'>",$body);
		}
		if(isset($this->article['style']['img'])){
			$body=str_replace('<img',"<img style='".$this->article['style']['img']."' ",$body);
		}
		if(isset($this->article['style']['a'])){
			$body=str_replace('<a',"<a style='".$this->article['style']['a']."' ",$body);
		}
		return $body;
	}
	function html2bbcode($text) {
		file_put_contents('a.txt',$text);
		$rules=array(
			"/<b[^>]*>([\s\S]*)<\/b>/U"								=>"[b]\\1[/b]",
			"/<strong[^>]*>([\s\S]*)<\/strong>/U"					=>"[b]\\1[/b]",
			"/<p[^>]*>[\s]*<\/p>/U"									=>"",
			"/<br\/?>/U"											=>"\n",
			"/\n+/"													=>"\n",
			"/<!--[\s\S]*-->/U"										=>""
		);
		$text=preg_replace(array_keys($rules),array_values($rules),$text);
		
		/*缩进*/
		preg_match('/text-indent:\s*(\d)em/',$this->article['style']['p'],$indent);
		$text=preg_replace('/<p([^>]+)>([\s\S]+?)<\/p>/',"<p\\1>".str_repeat(' ',intval($indent[1])*2)."\\2</p>",$text);
		/*字号和颜色*/
		$text = recursion('p', $text, 'ptag');
		/*行间距忽略*/
		
		/*图片对齐，先匹配对齐方式*/
		$img_align=array(
			'left'=>'margin: 10px 10px 10px 0',
			'center'=>'margin: 10px auto',
			'right'=>'margin: 10px 0 10px 10px'
		);
		foreach($img_align as $k=>$v){
			if(strpos($this->article['style']['img'],$v)!==false){
				$align=$k;break;
			}
		}
		/*获取所有图片，逐个替换*/
		preg_match_all("/<img([^>]*src[^>]*)>/iU",$text,$imgs);
		foreach($imgs[1] as $k=>$img){
			$text = str_replace($imgs[0][$k],"[align=$align]".imgtag($img)."[/align]",$text);
		}
		
		/*链接颜色和网址*/
		preg_match('/color:\s*([#0-9a-z]+)/i',$this->article['style']['a'],$acolor);
		$text=preg_replace("/<a[^>]*href=('|\")([\s\S]*)\\1[^>]*>([\s\S]*)<\/a>/U","[url=\\2][color={$acolor['1']}]\\3[/color][/url]",$text);
        
        return $text;
	}
	function setInnerLink($body){
		if($this->article['auto_inner_link']){			
			$tmptags = array();
			$poptemp = array();
			foreach($this->article['arc_tags'] as $tag){
				if($tag=='' || in_array($tag,$tmptags)) continue;	
				$tmptags[] = $tag;
				$result = DB::fetch_first("SELECT tagid FROM ".DB::table('common_tag')." WHERE tagname='$tag' and status=0");
				if($result['tagid']){
					$tags = DB::fetch_all("SELECT itemid FROM ".DB::table('common_tagitem')." WHERE tagid={$result['tagid']}");
				}
				if($tags){
					/*************/
					preg_match_all('/<a.*\/a>|<img.*>/isU',$body,$pop);
					foreach($pop[0] as $k => $v){
						$poptemp[0][] = md5($v);
						$poptemp[1][] = $v;
						$body = str_replace($v,md5($v),$body);
					}
					/*************/
					shuffle($tags);
					$arc = end($tags);
					if ($_G['setting']['rewritestatus'] && strpos("http".implode(',',$_G['setting']['rewritestatus']),"forum_viewthread")){
						$arc['titleurl']="/".str_replace(array('{page}','{prevpage}','{tid}'),array('1','1',$arc['itemid']),$_G['setting']['rewriterule']['forum_viewthread']);
					}
					else{
						$arc['titleurl']="/forum.php?mod=viewthread&tid=".$arc['itemid'];	
					}
					$body = preg_replace("/".preg_quote($tag)."/", "<a href='{$arc['titleurl']}' title='{$tag}' target='_blank'>$tag</a>", $body, 1);
				}
			}
			if(isset($poptemp[0]) && isset($poptemp[1]))
				$body = str_replace($poptemp[0],$poptemp[1],$body);
		}
		return $body;	
	}
	function checkAndInitData(){
		global $_G , $config;
		$fid = intval(getgpc('fid'));
		$sortid = intval(getgpc('sortid'));
		$typeid = intval(getgpc('typeid'));
		$special = intval(getgpc('special'));
		if(empty($sortid) && !empty($_POST['sortname'])){
			$_POST['sortid'] = intval($config['sortname'][trim($_POST['sortname'])]);
		}
		if(empty($typeid) && !empty($_POST['typename'])){
			$_POST['typeid'] = intval($config['typename'][trim($_POST['typename'])]);
		}
		if(empty($fid) && !empty($_GET['fname'])){
			$_GET['fid'] = intval($config['fname'][trim($_GET['fname'])]);
		}
		if(empty($_GET['fid']) || empty($_GET['subject']) || empty($_GET['message']) || empty($_GET['username'])){
			echo '错误缺少参数:';
			empty($_GET['fid']) && print('fid ');
			empty($_GET['subject']) && print('标题 ');
			empty($_GET['message']) && print('内容 ');
			empty($_GET['username']) && print('作者 ');
			exit();
		}else{
			$this->subject = trim($_POST['subject']);
 			$messageArr = explode('#p-a-g-e#', trim($_POST['message']));
			$this->message = array_shift($messageArr);
			$this->username = $_POST['username'];

			isset($_POST['price']) && $this->price = intval(trim($_POST['price']));
			isset($_POST['readperm']) && $this->readperm = intval($_POST['readperm']);
		}
		if(!empty($_POST['publishdate'])){
			$publish_date_arr = explode($config['分隔符'] , trim($_POST['publishdate']));
			$publish_date_tmp = array_shift($publish_date_arr);
			$publish_date = strtotime(trim($publish_date_tmp)); 
			if($publish_date > 0){
				$this->publish_date = $publish_date;
			}else{
				$this->publish_date = $_G['timestamp'];
			}
		}else{
			$this->publish_date = $_G['timestamp'];
		}
		
		if(!empty($_POST['avatar'])){
			$avatar_tmp_arr = array_unique(explode($config['分隔符'] , trim($_POST['avatar'])));
			foreach($avatar_tmp_arr as $k => $v){
				foreach ($_FILES as $k2 => $v2){
					if(strpos($k2 , 'avatar') === 0){
						if(strpos($v , $v2['name']) !== false){
							$this->avatar[] = array('username' => $v , 'file'=>$v2);
							break;
						}
					}
				}
			}
		}
		if(!empty($_FILES)){
			$i = 0;
			while (isset($_FILES['attach'.$i])){
				$this->attach[] = $_FILES['attach'.$i++];
			}
			//unset($_FILES);
			//$_FILES=null;
		}
		
		$this->save_date = $this->publish_date;
		if(!empty($messageArr)){
			if(!empty($usernameArr)){
				$reply_user = $usernameArr;
				$is_username_random = false;
			}elseif(!empty($config['回复用户名'])){
				$is_username_random = true;
				$reply_user = explode($config['分隔符'], $config['回复用户名']);
			}else{
				$reply_user = $this->username;
			}
			if(!empty($publish_date_arr)){
				$reply_times = $publish_date_arr;
			}
			//签名
			if(!empty($_POST['signature'])){
				$reply_sig = explode($config['分隔符'], trim($_POST['signature']));
				$this->sightml = array_shift($reply_sig);
				$is_signature_random = false;
			//随机用户签名
			}elseif(!empty($config['回复用户签名'])){
				$is_signature_random = true;
				$reply_sig = explode('|||', $config['回复用户签名']);
				$this->sightml = array_shift($reply_sig);
			}
			foreach($messageArr as $k => $v)
			{
				$reply_tmp 	= (!$is_username_random && isset($reply_user[$k])) ? $reply_user[$k] : $reply_user[rand(0,count($reply_user)-1)];
				$sig_tmp 	= (!$is_signature_random && isset($reply_sig[$k])) ? $reply_sig[$k] : (empty($reply_sig) ? '' : $reply_sig[rand(0,count($reply_sig)-1)]);
				if(isset($reply_times[$k])){
					$reply_time_tmp = strtotime($reply_times[$k]);
					if($reply_time_tmp > 0){
						 $this->save_date = $reply_time_tmp;
					}else{
						$this->save_date = $reply_time_tmp = $this->save_date + rand($config['回复间隔'][0] , $config['回复间隔'][1]);
					}
				}else{
					$this->save_date = $reply_time_tmp = $this->save_date + rand($config['回复间隔'][0] , $config['回复间隔'][1]);
				}
				$this->replys[$k] = array(
						'message' 		=>	$v , 
						'username'		=>	$reply_tmp ,
						'publishdate' 	=>	$reply_time_tmp ,
						'signature'		=>	$sig_tmp
					);
			}
		}
	}

	function randomip(){
		return rand(0,255).'.'.rand(0,255).'.'.rand(0,255).'.'.rand(0,255);
	}
	
	function logonUser(){
		$member = getuserbyuid($this->uid);
		$this->setloginstatus($member , 0);
	}
	
	function setloginstatus($member, $cookietime){
		global $_G;
		$_G['uid'] = intval($member['uid']);
		$_G['username'] = $member['username'];
		$_G['adminid'] = $member['adminid'];
		$_G['groupid'] = $member['groupid'];
		$_G['formhash'] = formhash();
		$_G['session']['invisible'] = getuserprofile('invisible');
		$_G['member'] = $member;
		
		loadcache('usergroup_'.$_G['groupid']);
		C::app()->session->isnew = true;
		/*
		C::app()->session->updatesession();
		dsetcookie('auth', authcode("{$member['password']}\t{$member['uid']}", 'ENCODE'), $cookietime, 1, true);
		dsetcookie('loginuser');
		dsetcookie('activationauth');
		dsetcookie('pmnum');
		*/
		include_once libfile('function/stat');
		updatestat('login', 1);
		$rule = updatecreditbyaction('daylogin', $_G['uid']);
		if(!$rule['updatecredit']) {
			checkusergroup($_G['uid']);
		}
		$_G['group']['allowsigbbcode'] = $_G['group']['allowhtml'] = $_G['group']['allowposttag'] = $_G['group']['allowsetattachperm'] = '1';
	}
	
	function checkGroup(){
		global $_G;
		if($_G['forum']['status'] == 3) {
			if(!helper_access::check_module('group')) {
				exit('错误group_status_off');
			}
			require_once libfile('function/group');
			$status = groupperm($_G['forum'], $_G['uid'], 'post');
			if($status == -1) {
				exit("错误|该版块不是{$_G['setting']['navs']['3']['navname']}");
			} elseif($status == 1) {
				exit("错误|该{$_G['setting']['navs']['3']['navname']}已关闭");
			} elseif($status == 2) {
				exit("错误|抱歉，您没有权限访问该{$_G['setting']['navs']['3']['navname']}");
			} elseif($status == 3) {
				exit('错误|请等待群主审核');
			} elseif($status == 4) {
				exit("错误|抱歉，您还不是本{$_G['setting']['navs']['3']['navname']}成员不能发帖");
			} elseif($status == 5) {
				exit('错误|请等待群主审核');
			}
		}
		if(($_G['forum']['simple'] & 1) || $_G['forum']['redirect']) {
			exit('错误|本版块禁止发帖');
		}
	}
	
	function upload($publishdate){
		global $_G;
		empty($publishdate) && $publishdate = $_G['timestamp'];
		$upload = new discuz_upload();
		$upload->init($_FILES['Filedata'], 'forum');
		//$this->attach = &$upload->attach;

		if($upload->error()) {
			exit('错误上传错误');
		}

		updatemembercount($_G['uid'], array('todayattachs' => 1, 'todayattachsize' => $upload->attach['size']));
		$upload->save();
		if($upload->error() == -103) {
			exit(8);
		} elseif($upload->error()) {
			exit(9);
		}
		$thumb = $remote = $width = 0;
		if($upload->attach['isimage']) {
			if($_G['setting']['showexif']) {
				require_once libfile('function/attachment');
				$exif = getattachexif(0, $upload->attach['target']);
			}
			if($_G['setting']['thumbsource'] || $_G['setting']['thumbstatus']) {
				require_once libfile('class/image');
				$image = new image;
			}
			if($_G['setting']['thumbsource'] && $_G['setting']['sourcewidth'] && $_G['setting']['sourceheight']) {
				$thumb = $image->Thumb($upload->attach['target'], '', $_G['setting']['sourcewidth'], $_G['setting']['sourceheight'], 1, 1) ? 1 : 0;
				$width = $image->imginfo['width'];
				$upload->attach['size'] = $image->imginfo['size'];
			}
			if($_G['setting']['thumbstatus']) {
				$thumb = $image->Thumb($upload->attach['target'], '', $_G['setting']['thumbwidth'], $_G['setting']['thumbheight'], $_G['setting']['thumbstatus'], 0) ? 1 : 0;
				$width = $image->imginfo['width'];
			}
			if($_G['setting']['thumbsource'] || !$_G['setting']['thumbstatus']) {
				list($width) = @getimagesize($upload->attach['target']);
			}
		}
		$aid = getattachnewaid($_G['uid']);
		$insert = array(
			'aid' => $aid,
			'dateline' => $publishdate,
			'filename' => censor($upload->attach['name']),
			'filesize' => $upload->attach['size'],
			'attachment' => $upload->attach['attachment'],
			'isimage' => $upload->attach['isimage'],
			'uid' => $_G['uid'],
			'thumb' => $thumb,
			'remote' => $remote,
			'width' => $width,
		);
		C::t('forum_attachment_unused')->insert($insert);
		if($upload->attach['isimage'] && $_G['setting']['showexif']) {
			C::t('forum_attachment_exif')->insert($aid, $exif);
		}
		return array('aid'=>$aid , 'name' => $upload->attach['name'] , 'isimage' => $upload->attach['isimage']);
	}
	
	function setUpdate(&$attach , $message){
		global $config;
		if(empty($attach)){
			return $message;
		}
		$uploadFile = array();
		foreach($attach as $k => $v){
			if(!empty($v) && $v['error'] == 0 && strpos($message , $v['name']) !== false){
				$_FILES['Filedata'] = $v;
				//$_FILES['Filedata']['name'] = addslashes(diconv(urldecode($_FILES['Filedata']['name']), 'UTF-8'));
				$_FILES['Filedata']['name'] = addslashes(urldecode($_FILES['Filedata']['name']));
				$_FILES['Filedata']['type'] = strrchr($_FILES['Filedata']['name'], '.');
				$uploadFile[] = $this->upload($this->publishdate);
				unset($attach[$k]);
			}
		}
		if(!empty($uploadFile)){
			foreach($uploadFile as $k => $v){
				$have = false;
				$preg_name = preg_quote($v['name']);
				if($v['isimage'] == '1'){
					//正则替换，
					if(preg_match("/<img[^<>]+".$preg_name."[^<>]+>/iUs", $message , $match)){
						$have = true;
						if($config['附件隐藏']){
							$message = str_replace($match[0], '[hide][attach]'.$v['aid'].'[/attach][/hide]', $message);
						}else{
							$message = str_replace($match[0], '[attach]'.$v['aid'].'[/attach]', $message);
						}
						//[img]19352073h2uh607pzo6oh6.jpg[/img]
					}elseif(preg_match("/\[img\][^\[\]]*".$preg_name."\[\/img\]/iUs", $message , $match)){
						$have = true;
						if($config['附件隐藏']){
							$message = str_replace($match[0], '[hide][attach]'.$v['aid'].'[/attach][/hide]', $message);
						}else{
							$message = str_replace($match[0], '[attach]'.$v['aid'].'[/attach]', $message);
						}
					}
				}else{
					if(preg_match("/<a[^<>]*>[^<>]*".$preg_name."[^<>]*</a>/iUs", $message , $match)){
						$have = true;
						if($config['附件隐藏']){
							$message = str_replace($match[0], '[hide][attach]'.$v['aid'].'[/attach][/hide]', $message);
						}else{
							$message = str_replace($match[0], '[attach]'.$v['aid'].'[/attach]', $message);
						}
					}elseif(preg_match("/\[url=[^\[\]]+\][^\[\]]*".$preg_name."\[\/url\]/iUs", $message , $match)){
						$have = true;
						if($config['附件隐藏']){
							$message = str_replace($match[0], '[hide][attach]'.$v['aid'].'[/attach][/hide]', $message);
						}else{
							$message = str_replace($match[0], '[attach]'.$v['aid'].'[/attach]', $message);
						}
					}
				}
				
				if(!empty($config['附件权限组'])){
					$readpermArr = explode('|', $config['附件权限组']);
					$readperm = $readpermArr[rand(0 , count($readpermArr)-1)];
				}else{
					$readperm = 0;
				}
				if(!empty($config['附件价格'])){
					$priceArr = explode('|', $config['附件价格']);
					$price = $priceArr[rand(0 , count($priceArr)-1)];
				}else{
					$price = 0;
				}
				$_GET['attachnew'][$v['aid']]['description'] = $v['name'];
				$_GET['attachnew'][$v['aid']]['readperm'] = $readperm;
				$_GET['attachnew'][$v['aid']]['price'] = $price;//$config['附件价格'];
			}
		}
		return $message;
	}
	
	function newthread(){
		global $_G,$config;
		require_once libfile('class/credit');
		require_once libfile('function/post');
		$pid = intval(getgpc('pid'));
		$sortid = intval(getgpc('sortid'));
		$typeid = intval(getgpc('typeid'));
		$special = intval(getgpc('special'));
		$postinfo = array('subject' => '');
		$thread = array('readperm' => '', 'pricedisplay' => '', 'hiddenreplies' => '');
		$_G['forum_dtype'] = $_G['forum_checkoption'] = $_G['forum_optionlist'] = $tagarray = $_G['forum_typetemplate'] = array();
		
		if($sortid) {
			require_once libfile('post/threadsorts', 'include');
		}
		$this->checkGroup();
		//require_once libfile('function/discuzcode');
		
		$space = array();
		space_merge($space, 'field_home');
		
		formulaperm($_G['forum']['formulaperm']);
		
		$_G['forum']['allowpostattach'] = isset($_G['forum']['allowpostattach']) ? $_G['forum']['allowpostattach'] : '';
		$_G['group']['allowpostattach'] = $_G['forum']['allowpostattach'] != -1 && ($_G['forum']['allowpostattach'] == 1 || (!$_G['forum']['postattachperm'] && $_G['group']['allowpostattach']) || ($_G['forum']['postattachperm'] && forumperm($_G['forum']['postattachperm'])));
		$_G['forum']['allowpostimage'] = isset($_G['forum']['allowpostimage']) ? $_G['forum']['allowpostimage'] : '';
		$_G['group']['allowpostimage'] = $_G['forum']['allowpostimage'] != -1 && ($_G['forum']['allowpostimage'] == 1 || (!$_G['forum']['postimageperm'] && $_G['group']['allowpostimage']) || ($_G['forum']['postimageperm'] && forumperm($_G['forum']['postimageperm'])));
		$_G['group']['attachextensions'] = $_G['forum']['attachextensions'] ? $_G['forum']['attachextensions'] : $_G['group']['attachextensions'];
		require_once libfile('function/upload');
		$swfconfig = getuploadconfig($_G['uid'], $_G['fid']);
		$imgexts = str_replace(array(';', '*.'), array(', ', ''), $swfconfig['imageexts']['ext']);
		$allowuploadnum = $allowuploadtoday = TRUE;
		if($_G['group']['allowpostattach'] || $_G['group']['allowpostimage']) {
			if($_G['group']['maxattachnum']) {
				$allowuploadnum = $_G['group']['maxattachnum'] - getuserprofile('todayattachs');
				$allowuploadnum = $allowuploadnum < 0 ? 0 : $allowuploadnum;
				if(!$allowuploadnum) {
					$allowuploadtoday = false;
				}
			}
			if($_G['group']['maxsizeperday']) {
				$allowuploadsize = $_G['group']['maxsizeperday'] - getuserprofile('todayattachsize');
				$allowuploadsize = $allowuploadsize < 0 ? 0 : $allowuploadsize;
				if(!$allowuploadsize) {
					$allowuploadtoday = false;
				}
				$allowuploadsize = $allowuploadsize / 1048576 >= 1 ? round(($allowuploadsize / 1048576), 1).'MB' : round(($allowuploadsize / 1024)).'KB';
			}
		}
		$allowpostimg = $_G['group']['allowpostimage'] && $imgexts;
		$enctype = ($_G['group']['allowpostattach'] || $_G['group']['allowpostimage']) ? 'enctype="multipart/form-data"' : '';
		$maxattachsize_mb = $_G['group']['maxattachsize'] / 1048576 >= 1 ? round(($_G['group']['maxattachsize'] / 1048576), 1).'MB' : round(($_G['group']['maxattachsize'] / 1024)).'KB';
		
		$_G['group']['maxprice'] = isset($_G['setting']['extcredits'][$_G['setting']['creditstrans']]) ? $_G['group']['maxprice'] : 0;
		
		$extra =  '';
		
		$subject = dhtmlspecialchars(censor(trim($this->subject)));
		$subject = !empty($subject) ? str_replace("\t", ' ', $subject) : $subject;

		$message = $this->setUpdate($this->attach , censor($this->message));
		$polloptions = isset($polloptions) ? censor(trim($polloptions)) : '';
		$readperm = isset($this->readperm) ? $this->readperm : 0;
		$price = isset($this->price) ? $this->price : 0;
		/*
		if(empty($bbcodeoff) && !$_G['group']['allowhidecode'] && !empty($message) && preg_match("/\[hide=?\d*\].*?\[\/hide\]/is", preg_replace("/(\[code\](.+?)\[\/code\])/is", ' ', $message))) {
			exit('错误post_hide_nopermission');
		}
		*/
		if ($this->article['arcrank']){
			$modnewthreads = $modnewreplies = 0;	
		}else{
			$modnewthreads = $modnewreplies = "-2";
		}
		$urloffcheck = $usesigcheck = $smileyoffcheck = $codeoffcheck = $htmloncheck = $emailcheck = '';
		
		$seccodecheck = ($_G['setting']['seccodestatus'] & 4) && (!$_G['setting']['seccodedata']['minposts'] || getuserprofile('posts') < $_G['setting']['seccodedata']['minposts']);
		$secqaacheck = $_G['setting']['secqaa']['status'] & 2 && (!$_G['setting']['secqaa']['minposts'] || getuserprofile('posts') < $_G['setting']['secqaa']['minposts']);
		
		$_G['group']['allowpostpoll'] = $_G['group']['allowpost'] && $_G['group']['allowpostpoll'] && ($_G['forum']['allowpostspecial'] & 1);
		$_G['group']['allowposttrade'] = $_G['group']['allowpost'] && $_G['group']['allowposttrade'] && ($_G['forum']['allowpostspecial'] & 2);
		$_G['group']['allowpostreward'] = $_G['group']['allowpost'] && $_G['group']['allowpostreward'] && ($_G['forum']['allowpostspecial'] & 4);
		$_G['group']['allowpostactivity'] = $_G['group']['allowpost'] && $_G['group']['allowpostactivity'] && ($_G['forum']['allowpostspecial'] & 8);
		$_G['group']['allowpostdebate'] = $_G['group']['allowpost'] && $_G['group']['allowpostdebate'] && ($_G['forum']['allowpostspecial'] & 16);
		$_G['forum']['threadplugin'] = dunserialize($_G['forum']['threadplugin']);
		

		$_G['group']['allowanonymous'] = $_G['forum']['allowanonymous'] || $_G['group']['allowanonymous'] ? 1 : 0;
		
		$policykey = 'post';
		$postcredits = $_G['forum'][$policykey.'credits'] ? $_G['forum'][$policykey.'credits'] : $_G['setting']['creditspolicy'][$policykey];
		
		
		$albumlist = array();
		if(helper_access::check_module('album') && $_G['group']['allowupload'] && $_G['uid']) {
			$query = C::t('home_album')->fetch_all_by_uid($_G['uid'], 'updatetime');
			foreach($query as $value) {
				if($value['picnum']) {
					$albumlist[] = $value;
				}
			}
		}
		$this->check_allow_action('allowpost');
		
		if(helper_access::check_module('album') && $_G['group']['allowupload'] && $_G['setting']['albumcategorystat'] && !empty($_G['cache']['albumcategory'])) {
			require_once libfile('function/portalcp');
		}
		loadcache('groupreadaccess');
		
		$_GET['save'] = $_G['uid'] ? $_GET['save'] : 0;
		if ($this->publish_date  > $_G['timestamp']) {
			$_GET['save'] = 1;
		}
		$publishdate = $this->publish_date;
		
		$typeid = isset($typeid) && isset($_G['forum']['threadtypes']['types'][$typeid]) && (empty($_G['forum']['threadtypes']['moderators'][$typeid]) || $_G['forum']['ismoderator']) ? $typeid : 0;
		//$displayorder = ($_G['forum']['ismoderator'] && $_G['group']['allowstickthread'] && !empty($_GET['sticktopic'])) ? 1 : (empty($_GET['save']) ? 0 : -4);
		$displayorder=$this->article['arcrank']?0:-2;
		if($displayorder == -4) {
			$_GET['addfeed'] = 0;
		}
		$digest = $_G['forum']['ismoderator'] && $_G['group']['allowdigestthread'] && !empty($_GET['addtodigest']) ? 1 : 0;
		$readperm = $_G['group']['allowsetreadperm'] ? $readperm : 0;
		$isanonymous = 0;
		//$price = $_G['group']['maxprice'] && !$special ? ($price <= $_G['group']['maxprice'] ? $price : $_G['group']['maxprice']) : 0;
	
		if(!$typeid && $_G['forum']['threadtypes']['required'] && !$special) {
			exit('错误该板块必须要主题分类typeid');
		}
	
		if(!$sortid && $_G['forum']['threadsorts']['required'] && !$special) {
			exit('错误该板块必须要分类信息sortid');
		}
	
		if($price > 0 && floor($price * (1 - $_G['setting']['creditstax'])) == 0) {
			exit('错误|抱歉，您的主题售价扣除积分交易税后为 0|post_net_price_iszero');
		}
	
		$typeexpiration = intval($_GET['typeexpiration']);
	
		if($_G['forum']['threadsorts']['expiration'][$typeid] && !$typeexpiration) {
			exit('错误|抱歉，此主题必须指定有效期|threadtype_expiration_invalid');//);
		}
	
		$_G['forum_optiondata'] = array();
		if($_G['forum']['threadsorts']['types'][$sortid] && !$_G['forum']['allowspecialonly']) {
			$_G['forum_optiondata'] = threadsort_validator($_GET['typeoption'], $pid);
		}
	
		$author =  $_G['username'] ;
	
		$moderated = $digest || $displayorder > 0 ? 1 : 0;
	
		$thread['status'] = 0;
		$isgroup = $_G['forum']['status'] == 3 ? 1 : 0;
	
		//回复奖励
		if($_G['group']['allowreplycredit']) {
			$_GET['replycredit_extcredits'] = intval($_GET['replycredit_extcredits']);
			$_GET['replycredit_times'] = intval($_GET['replycredit_times']);
			$_GET['replycredit_membertimes'] = intval($_GET['replycredit_membertimes']);
			$_GET['replycredit_random'] = intval($_GET['replycredit_random']);
	
			$_GET['replycredit_random'] = $_GET['replycredit_random'] < 0 || $_GET['replycredit_random'] > 99 ? 0 : $_GET['replycredit_random'] ;
			$replycredit = $replycredit_real = 0;
			if($_GET['replycredit_extcredits'] > 0 && $_GET['replycredit_times'] > 0) {
				$replycredit_real = ceil(($_GET['replycredit_extcredits'] * $_GET['replycredit_times']) + ($_GET['replycredit_extcredits'] * $_GET['replycredit_times'] *  $_G['setting']['creditstax']));
				if($replycredit_real > getuserprofile('extcredits'.$_G['setting']['creditstransextra'][10])) {
					exit('错误|抱歉，回帖送积分额度大于您的积分|replycredit_morethan_self');
				} else {
					$replycredit = ceil($_GET['replycredit_extcredits'] * $_GET['replycredit_times']);
				}
			}
		}
		
		$newthread = array(
			'fid' => $_G['fid'],
			'posttableid' => 0,
			'readperm' => $readperm,
			'price' => $price,
			'typeid' => $typeid,
			'sortid' => $sortid,
			'author' => $author,
			'authorid' => $_G['uid'],
			'subject' => $subject,
			'views'	 =>	(rand(5,10)*count($this->replys))+rand(1,10),
			'dateline' => $publishdate,
			'lastpost' => $publishdate,
			'lastposter' => $author,
			'displayorder' => $displayorder,
			'digest' => $digest,
			'special' => $special,
			'attachment' => 0,
			'moderated' => $moderated,
			'status' => $thread['status'],
			'isgroup' => $isgroup,
			'replycredit' => $replycredit,
			'closed' => $closed ? 1 : 0
		);
		$tid = C::t('forum_thread')->insert($newthread, true);
		useractionlog($_G['uid'], 'tid');
	
		if(!getuserprofile('threads') && $_G['setting']['newbie']) {
			C::t('forum_thread')->update($tid, array('icon' => $_G['setting']['newbie']));
		}
		//计划发布
		if ($publishdate != $_G['timestamp']) {
			loadcache('cronpublish');
			$cron_publish_ids = dunserialize($_G['cache']['cronpublish']);
			$cron_publish_ids[$tid] = $tid;
			$cron_publish_ids = serialize($cron_publish_ids);
			savecache('cronpublish', $cron_publish_ids);
		}
	
		
		C::t('common_member_field_home')->update($_G['uid'], array('recentnote'=>$subject));
		
		if($moderated) {
			updatemodlog($tid, ($displayorder > 0 ? 'STK' : 'DIG'));
			updatemodworks(($displayorder > 0 ? 'STK' : 'DIG'), 1);
		}
	
		if($_G['forum']['threadsorts']['types'][$sortid] && !empty($_G['forum_optiondata']) && is_array($_G['forum_optiondata'])) {
			$filedname = $valuelist = $separator = '';
			foreach($_G['forum_optiondata'] as $optionid => $value) {
				if($value) {
					$filedname .= $separator.$_G['forum_optionlist'][$optionid]['identifier'];
					$valuelist .= $separator."'".daddslashes($value)."'";
					$separator = ' ,';
				}
	
				if($_G['forum_optionlist'][$optionid]['type'] == 'image') {
					$identifier = $_G['forum_optionlist'][$optionid]['identifier'];
					$sortaids[] = intval($_GET['typeoption'][$identifier]['aid']);
				}
	
				C::t('forum_typeoptionvar')->insert(array(
					'sortid' => $sortid,
					'tid' => $tid,
					'fid' => $_G['fid'],
					'optionid' => $optionid,
					'value' => censor($value),
					'expiration' => ($typeexpiration ? $publishdate + $typeexpiration : 0),
				));
			}
	
			if($filedname && $valuelist) {
				C::t('forum_optionvalue')->insert($sortid, "($filedname, tid, fid) VALUES ($valuelist, '$tid', '$_G[fid]')");
			}
		}
		if($_G['group']['allowat']) {
			$atlist = $atlist_tmp = array();
			preg_match_all("/@([^\r\n]*?)\s/i", $message.' ', $atlist_tmp);
			$atlist_tmp = array_slice(array_unique($atlist_tmp[1]), 0, $_G['group']['allowat']);
			if(!empty($atlist_tmp)) {
				if(empty($_G['setting']['at_anyone'])) {
					foreach(C::t('home_follow')->fetch_all_by_uid_fusername($_G['uid'], $atlist_tmp) as $row) {
						$atlist[$row['followuid']] = $row['fusername'];
					}
					if(count($atlist) < $_G['group']['allowat']) {
						$query = C::t('home_friend')->fetch_all_by_uid_username($_G['uid'], $atlist_tmp);
						foreach($query as $row) {
							$atlist[$row['fuid']] = $row['fusername'];
						}
					}
				} else {
					foreach(C::t('common_member')->fetch_all_by_username($atlist_tmp) as $row) {
						$atlist[$row['uid']] = $row['username'];
					}
				}
			}
			if($atlist) {
				foreach($atlist as $atuid => $atusername) {
					$atsearch[] = "/@$atusername /i";
					$atreplace[] = "[url=home.php?mod=space&uid=$atuid]@{$atusername}[/url] ";
				}
				$message = preg_replace($atsearch, $atreplace, $message.' ', 1);
			}
		}
	
		$bbcodeoff = checkbbcodes($message, 0);
		$smileyoff = checksmilies($message, 0);
		$parseurloff = !empty($_GET['parseurloff']);
		$htmlon = strpos($message , '<') !==false ? 1 : 0;
		$usesig = $config['使用签名'];
		$tagstr = $this->add_tag($_POST['tags'], $tid);
		
		if($_G['group']['allowreplycredit']) {
			if($replycredit > 0 && $replycredit_real > 0) {
				updatemembercount($_G['uid'], array('extcredits'.$_G['setting']['creditstransextra'][10] => -$replycredit_real), 1, 'RCT', $tid);
				$insertdata = array(
						'tid' => $tid,
						'extcredits' => $_GET['replycredit_extcredits'],
						'extcreditstype' => $_G['setting']['creditstransextra'][10],
						'times' => $_GET['replycredit_times'],
						'membertimes' => $_GET['replycredit_membertimes'],
						'random' => $_GET['replycredit_random']
					);
				C::t('forum_replycredit')->insert($insertdata);
			}
		}
		$pinvisible = $modnewthreads ? -2 : (empty($_GET['save']) ? 0 : -3);

		$pid = insertpost(array(
			'fid' => $_G['fid'],
			'tid' => $tid,
			'first' => '1',
			'author' => $_G['username'],
			'authorid' => $_G['uid'],
			'subject' => $subject,
			'dateline' => $publishdate,
			'message' => $message,
			'useip' => $_G['clientip'],
			'invisible' => $pinvisible,
			'anonymous' => $isanonymous,
			'usesig' => $usesig,
			'htmlon' => $htmlon,
			'bbcodeoff' => $bbcodeoff,
			'smileyoff' => $smileyoff,
			'parseurloff' => $parseurloff,
			'attachment' => '0',
			'tags' => $tagstr,
			'replycredit' => 0,
			'status' => 0
		));
		if($_G['group']['allowat'] && $atlist) {
			foreach($atlist as $atuid => $atusername) {
				notification_add($atuid, 'at', 'at_message', array('from_id' => $tid, 'from_idtype' => 'thread', 'buyerid' => $_G['uid'], 'buyer' => $_G['username'], 'tid' => $tid, 'subject' => $subject, 'pid' => $pid, 'message' => messagecutstr($message, 150)));
			}
			set_atlist_cookie(array_keys($atlist));
		}
		$threadimageaid = 0;
		$threadimage = array();

		if($_G['forum']['threadsorts']['types'][$sortid] && !empty($_G['forum_optiondata']) && is_array($_G['forum_optiondata']) && $sortaids) {
			foreach($sortaids as $sortaid) {
				convertunusedattach($sortaid, $tid, $pid);
			}
		}
	
		if(($_G['group']['allowpostattach'] || $_G['group']['allowpostimage']) && ($_GET['attachnew'] || $sortid || !empty($_GET['activityaid']))) {
			updateattach($displayorder == -4 || $modnewthreads, $tid, $pid, $_GET['attachnew']);
			if(!$threadimageaid) {
				$threadimage = C::t('forum_attachment_n')->fetch_max_image('tid:'.$tid, 'tid', $tid);
				$threadimageaid = $threadimage['aid'];
			}
		}
	
		$values = array('fid' => $_G['fid'], 'tid' => $tid, 'pid' => $pid, 'coverimg' => '');
		$param = array();
		if($_G['forum']['picstyle']) {
			if(!setthreadcover($pid, 0, $threadimageaid)) {
				preg_match_all("/(\[img\]|\[img=\d{1,4}[x|\,]\d{1,4}\])\s*([^\[\<\r\n]+?)\s*\[\/img\]/is", $message, $imglist, PREG_SET_ORDER);
				$values['coverimg'] = "<p id=\"showsetcover\">".lang('message', 'post_newthread_set_cover')."<span id=\"setcoverwait\"></span></p><script>if($('forward_a')){\$('forward_a').style.display='none';setTimeout(\"$('forward_a').style.display=''\", 5000);};ajaxget('forum.php?mod=ajax&action=setthreadcover&tid=$tid&pid=$pid&fid=$_G[fid]&imgurl={$imglist[0][2]}&newthread=1', 'showsetcover', 'setcoverwait')</script>";
				$param['clean_msgforward'] = 1;
				$param['timeout'] = $param['refreshtime'] = 15;
			}
		}
	
		if($threadimageaid) {
			if(!$threadimage) {
				$threadimage = C::t('forum_attachment_n')->fetch('tid:'.$tid, $threadimageaid);
			}
			$threadimage = daddslashes($threadimage);
			C::t('forum_threadimage')->insert(array(
				'tid' => $tid,
				'attachment' => $threadimage['attachment'],
				'remote' => $threadimage['remote'],
			));
		}
	
		$statarr = array(0 => 'thread', 1 => 'poll', 2 => 'trade', 3 => 'reward', 4 => 'activity', 5 => 'debate', 127 => 'thread');
		include_once libfile('function/stat');
		updatestat($isgroup ? 'groupthread' : 'thread');
	
		//dsetcookie('clearUserdata', 'forum');
	
		if($modnewthreads) {
			updatemoderate('tid', $tid);
			C::t('forum_forum')->update_forum_counter($_G['fid'], 0, 0, 1);
			manage_addnotify('verifythread');
			exit('Article Saved!');
		} else {
			if($displayorder != -4) {
				if($digest) {
					updatepostcredits('+',  $_G['uid'], 'digest', $_G['fid']);
				}
				updatepostcredits('+',  $_G['uid'], 'post', $_G['fid']);
				if($isgroup) {
					C::t('forum_groupuser')->update_counter_for_user($_G['uid'], $_G['fid'], 1);
				}
	
				C::t('forum_forum')->update_forum_counter($_G['fid'], 1, 1, 1);
				if($publishdate >= $_G['timestamp']){
					$subject = str_replace("\t", ' ', $subject);
					$lastpost = "$tid\t".$subject."\t$publishdate\t$author";
					C::t('forum_forum')->update($_G['fid'], array('lastpost' => $lastpost));
					if($_G['forum']['type'] == 'sub') {
						C::t('forum_forum')->update($_G['forum']['fup'], array('lastpost' => $lastpost));
					}
				}
			}

			if($_G['forum']['status'] == 3) {
				if($publishdate >= $_G['timestamp']){
					C::t('forum_forumfield')->update($_G['fid'], array('lastupdate' => $publishdate));
				}
				require_once libfile('function/grouplog');
				updategroupcreditlog($_G['fid'], $_G['uid']);
			}
		}
		
		if ($_G['setting']['rewritestatus']&&strpos("http".implode(',',$_G['setting']['rewritestatus']),"forum_viewthread")){
			$this->titleurl="/".str_replace(array('{page}','{prevpage}','{tid}'),array('1','1',$tid),$_G['setting'][rewriterule][forum_viewthread]);
		}else{	
			$this->titleurl="/forum.php?mod=viewthread&tid=".$tid;	
		}

		$_G['tid'] = $_GET['tid'] = $tid;
		$_GET['attachnew'] = null;
		loadforum();
	}
	
	function doreply($username , $message , $publishdate){
		global $_G,$config;
		//cknewuser();
		$pid = intval(getgpc('pid'));
		$sortid = intval(getgpc('sortid'));
		$typeid = intval(getgpc('typeid'));
		$special = intval(getgpc('special'));
		
		$postinfo = array('subject' => '');
		$thread = array('readperm' => '', 'pricedisplay' => '', 'hiddenreplies' => '');
		
		$_G['forum_dtype'] = $_G['forum_checkoption'] = $_G['forum_optionlist'] = $tagarray = $_G['forum_typetemplate'] = array();
		
		if($sortid) {
			require_once libfile('post/threadsorts', 'include');
		}
		$this->checkGroup();
		require_once libfile('function/discuzcode');
		
		$space = array();
		space_merge($space, 'field_home');
		
		if(!empty($_GET['cedit'])) {
			unset($_G['inajax'], $_GET['infloat'], $_GET['ajaxtarget'], $_GET['handlekey']);
		}
		
		$thread = C::t('forum_thread')->fetch($_G['tid']);
		formulaperm($_G['forum']['formulaperm']);
		
		$_G['forum']['allowpostattach'] = isset($_G['forum']['allowpostattach']) ? $_G['forum']['allowpostattach'] : '';
		$_G['group']['allowpostattach'] = $_G['forum']['allowpostattach'] != -1 && ($_G['forum']['allowpostattach'] == 1 || (!$_G['forum']['postattachperm'] && $_G['group']['allowpostattach']) || ($_G['forum']['postattachperm'] && forumperm($_G['forum']['postattachperm'])));
		$_G['forum']['allowpostimage'] = isset($_G['forum']['allowpostimage']) ? $_G['forum']['allowpostimage'] : '';
		$_G['group']['allowpostimage'] = $_G['forum']['allowpostimage'] != -1 && ($_G['forum']['allowpostimage'] == 1 || (!$_G['forum']['postimageperm'] && $_G['group']['allowpostimage']) || ($_G['forum']['postimageperm'] && forumperm($_G['forum']['postimageperm'])));
		$_G['group']['attachextensions'] = $_G['forum']['attachextensions'] ? $_G['forum']['attachextensions'] : $_G['group']['attachextensions'];
		require_once libfile('function/upload');
		$swfconfig = getuploadconfig($_G['uid'], $_G['fid']);
		$imgexts = str_replace(array(';', '*.'), array(', ', ''), $swfconfig['imageexts']['ext']);
		$allowuploadnum = $allowuploadtoday = TRUE;
		if($_G['group']['allowpostattach'] || $_G['group']['allowpostimage']) {
			if($_G['group']['maxattachnum']) {
				$allowuploadnum = $_G['group']['maxattachnum'] - getuserprofile('todayattachs');
				$allowuploadnum = $allowuploadnum < 0 ? 0 : $allowuploadnum;
				if(!$allowuploadnum) {
					$allowuploadtoday = false;
				}
			}
			if($_G['group']['maxsizeperday']) {
				$allowuploadsize = $_G['group']['maxsizeperday'] - getuserprofile('todayattachsize');
				$allowuploadsize = $allowuploadsize < 0 ? 0 : $allowuploadsize;
				if(!$allowuploadsize) {
					$allowuploadtoday = false;
				}
				$allowuploadsize = $allowuploadsize / 1048576 >= 1 ? round(($allowuploadsize / 1048576), 1).'MB' : round(($allowuploadsize / 1024)).'KB';
			}
		}
		$allowpostimg = $_G['group']['allowpostimage'] && $imgexts;
		$enctype = ($_G['group']['allowpostattach'] || $_G['group']['allowpostimage']) ? 'enctype="multipart/form-data"' : '';
		$maxattachsize_mb = $_G['group']['maxattachsize'] / 1048576 >= 1 ? round(($_G['group']['maxattachsize'] / 1048576), 1).'MB' : round(($_G['group']['maxattachsize'] / 1024)).'KB';
		
		$_G['group']['maxprice'] = isset($_G['setting']['extcredits'][$_G['setting']['creditstrans']]) ? $_G['group']['maxprice'] : 0;
		
		$extra = '';
		
		$subject = isset($subject) ? dhtmlspecialchars(censor(trim($subject))) : '';
		$subject = !empty($subject) ? str_replace("\t", ' ', $subject) : $subject;
		$message = isset($message) ? censor($message) : '';
		/*
		if(empty($bbcodeoff) && !$_G['group']['allowhidecode'] && !empty($message) && preg_match("/\[hide=?\d*\].*?\[\/hide\]/is", preg_replace("/(\[code\](.+?)\[\/code\])/is", ' ', $message))) {
			exit('错误post_hide_nopermission');//);
		}
		*/
		if ($this->article['arcrank']){
			$modnewthreads = $modnewreplies = 0;	
		}else{
			$modnewthreads = $modnewreplies = "-2";
		}
		
		$urloffcheck = $usesigcheck = $smileyoffcheck = $codeoffcheck = $htmloncheck = $emailcheck = '';
		
		$seccodecheck = ($_G['setting']['seccodestatus'] & 4) && (!$_G['setting']['seccodedata']['minposts'] || getuserprofile('posts') < $_G['setting']['seccodedata']['minposts']);
		$secqaacheck = $_G['setting']['secqaa']['status'] & 2 && (!$_G['setting']['secqaa']['minposts'] || getuserprofile('posts') < $_G['setting']['secqaa']['minposts']);
		
		$_G['group']['allowpostpoll'] = $_G['group']['allowpost'] && $_G['group']['allowpostpoll'] && ($_G['forum']['allowpostspecial'] & 1);
		$_G['group']['allowposttrade'] = $_G['group']['allowpost'] && $_G['group']['allowposttrade'] && ($_G['forum']['allowpostspecial'] & 2);
		$_G['group']['allowpostreward'] = $_G['group']['allowpost'] && $_G['group']['allowpostreward'] && ($_G['forum']['allowpostspecial'] & 4);
		$_G['group']['allowpostactivity'] = $_G['group']['allowpost'] && $_G['group']['allowpostactivity'] && ($_G['forum']['allowpostspecial'] & 8);
		$_G['group']['allowpostdebate'] = $_G['group']['allowpost'] && $_G['group']['allowpostdebate'] && ($_G['forum']['allowpostspecial'] & 16);
		$_G['forum']['threadplugin'] = dunserialize($_G['forum']['threadplugin']);

		$_G['group']['allowanonymous'] = $_G['forum']['allowanonymous'] || $_G['group']['allowanonymous'] ? 1 : 0;
		$policykey = 'reply';
		if($policykey) {
			$postcredits = $_G['forum'][$policykey.'credits'] ? $_G['forum'][$policykey.'credits'] : $_G['setting']['creditspolicy'][$policykey];
		}
		
		$albumlist = array();
		if(helper_access::check_module('album') && $_G['group']['allowupload'] && $_G['uid']) {
			$query = C::t('home_album')->fetch_all_by_uid($_G['uid'], 'updatetime');
			foreach($query as $value) {
				if($value['picnum']) {
					$albumlist[] = $value;
				}
			}
		}
		
		$this->check_allow_action('allowreply');
		
		if(helper_access::check_module('album') && $_G['group']['allowupload'] && $_G['setting']['albumcategorystat'] && !empty($_G['cache']['albumcategory'])) {
			require_once libfile('function/portalcp');
		}
				
		require_once libfile('function/forumlist');
		
		$isfirstpost = 0;
		$showthreadsorts = 0;
		$quotemessage = '';
		if(trim($subject) == '' && trim($message) == '') {
			return false;
		}
		$attentionon = empty($_GET['attention_add']) ? 0 : 1;
		$attentionoff = empty($attention_remove) ? 0 : 1;
		$heatthreadset = update_threadpartake($_G['tid'], true);
		$message = $this->setUpdate($this->attach , $message);
		$bbcodeoff = checkbbcodes($message, 0);
		$smileyoff = checksmilies($message, 0);
		$parseurloff = !empty($_GET['parseurloff']);
		$htmlon = strpos($message , '<') !== false ? 1 : 0 ;//$_G['group']['allowhtml'] && !empty($_GET['htmlon']) ? 1 : 0;
		$usesig = $config['使用签名'];
	
		$isanonymous = 0;
		$author = $_G['username'] ;
	
		if($thread['displayorder'] == -4) {
			$modnewreplies = 0;
		}
		$pinvisible = $modnewreplies ? -2 : ($thread['displayorder'] == -4 ? -3 : 0);
		//$pinvisible = $modnewreplies ? -2 : ($thread['displayorder'] == -4 ? -3 : 0);
		$postcomment = in_array(2, $_G['setting']['allowpostcomment']) && $_G['group']['allowcommentreply'] && !$pinvisible && !empty($_GET['reppid']) && ($nauthorid != $_G['uid'] || $_G['setting']['commentpostself']) ? messagecutstr($message, 200, ' ') : '';
		$position= C::t('forum_post')->count_visiblepost_by_tid($_G['tid']) + 1;
		$pid = insertpost(array(
			'fid' => $_G['fid'],
			'tid' => $_G['tid'],
			'first' => '0',
			'author' => $_G['username'],
			'authorid' => $_G['uid'],
			'subject' => $subject,
			'dateline' => $publishdate,
			'message' => $message,
			'useip' => $this->randomip(),
			'invisible' => $pinvisible,
			'anonymous' => $isanonymous,
			'usesig' => $usesig,
			'htmlon' => $htmlon,
			'bbcodeoff' => $bbcodeoff,
			'smileyoff' => $smileyoff,
			'parseurloff' => $parseurloff,
			'attachment' => '0',
			'status' => 0,
			'position' => $position
		));
		if($_G['group']['allowat'] && $atlist) {
			foreach($atlist as $atuid => $atusername) {
				notification_add($atuid, 'at', 'at_message', array('from_id' => $_G['tid'], 'from_idtype' => 'thread', 'buyerid' => $_G['uid'], 'buyer' => $_G['username'], 'tid' => $_G['tid'], 'subject' => $thread['subject'], 'pid' => $pid, 'message' => messagecutstr($message, 150)));
			}
			set_atlist_cookie(array_keys($atlist));
		}
		$updatethreaddata = $heatthreadset ? $heatthreadset : array();
		$postionid = C::t('forum_post')->fetch_maxposition_by_tid($thread['posttableid'], $_G['tid']);
		$updatethreaddata[] = DB::field('maxposition', $postionid);
		if(getstatus($thread['status'], 3) && $postionid) {
			$rushstopfloor = $rushinfo['stopfloor'];
			if($rushstopfloor > 0 && $thread['closed'] == 0 && $postionid >= $rushstopfloor) {
				$updatethreaddata[] = 'closed=1';
			}
		}
		useractionlog($_G['uid'], 'pid');
	
		$nauthorid = 0;
	
		if($thread['authorid'] != $_G['uid'] && getstatus($thread['status'], 6) && empty($_GET['noticeauthor']) && !$isanonymous && !$modnewreplies) {
			$thapost = C::t('forum_post')->fetch_threadpost_by_tid_invisible($_G['tid'], 0);
			notification_add($thapost['authorid'], 'post', 'reppost_noticeauthor', array(
				'tid' => $thread['tid'],
				'subject' => $thread['subject'],
				'fid' => $_G['fid'],
				'pid' => $pid,
				'from_id' => $thread['tid'],
				'from_idtype' => 'post',
			));
		}
	
		if($thread['replycredit'] > 0 && !$modnewreplies && $thread['authorid'] != $_G['uid'] && $_G['uid']) {
	
			$replycredit_rule = C::t('forum_replycredit')->fetch($_G['tid']);
			if(!empty($replycredit_rule['times'])) {
				$have_replycredit = C::t('common_credit_log')->count_by_uid_operation_relatedid($_G['uid'], 'RCA', $_G['tid']);
				if($replycredit_rule['membertimes'] - $have_replycredit > 0 && $thread['replycredit'] - $replycredit_rule['extcredits'] >= 0) {
					$replycredit_rule['extcreditstype'] = $replycredit_rule['extcreditstype'] ? $replycredit_rule['extcreditstype'] : $_G['setting']['creditstransextra'][10];
					if($replycredit_rule['random'] > 0) {
						$rand = rand(1, 100);
						$rand_replycredit = $rand <= $replycredit_rule['random'] ? true : false ;
					} else {
						$rand_replycredit = true;
					}
					if($rand_replycredit) {
						updatemembercount($_G['uid'], array($replycredit_rule['extcreditstype'] => $replycredit_rule['extcredits']), 1, 'RCA', $_G[tid]);
						C::t('forum_post')->update('tid:'.$_G['tid'], $pid, array('replycredit' => $replycredit_rule['extcredits']));
						$updatethreaddata[] = DB::field('replycredit', $thread['replycredit'] - $replycredit_rule['extcredits']);
					}
				}
			}
		}
		($_G['group']['allowpostattach'] || $_G['group']['allowpostimage']) && ($_GET['attachnew'] || $special == 2 && $_GET['tradeaid']) && updateattach($thread['displayorder'] == -4 || $modnewreplies, $_G['tid'], $pid, $_GET['attachnew']);
		$_GET['attachnew'] = null;
		$_G['forum']['threadcaches'] && deletethreadcaches($_G['tid']);
	
		include_once libfile('function/stat');
		updatestat($thread['isgroup'] ? 'grouppost' : 'post');
	
		$param = array('fid' => $_G['fid'], 'tid' => $_G['tid'], 'pid' => $pid, 'from' => $_GET['from'], 'sechash' => !empty($_GET['sechash']) ? $_GET['sechash'] : '');
		if($feedid) {
			$param['feedid'] = $feedid;
		}
		//dsetcookie('clearUserdata', 'forum');
	
		if($modnewreplies) {
			updatemoderate('pid', $pid);
			unset($param['pid']);
			if($updatethreaddata) {
				C::t('forum_thread')->update($_G['tid'], $updatethreaddata, false, false, 0, true);
			}
			C::t('forum_forum')->update_forum_counter($_G['fid'], 0, 0, 1, 1);
			manage_addnotify('verifypost');
		} else {
			$fieldarr = array(
				'lastposter' => array($author),
				'replies' => 1
			);
			if($thread['lastpost'] < $publishdate) {
				$fieldarr['lastpost'] = array($publishdate);
			}
			$row = C::t('forum_threadaddviews')->fetch($_G['tid']);
			if(!empty($row)) {
				C::t('forum_threadaddviews')->update($_G['tid'], array('addviews' => 0));
				$fieldarr['views'] = $row['addviews'];
			}
			$updatethreaddata = array_merge($updatethreaddata, C::t('forum_thread')->increase($_G['tid'], $fieldarr, false, 0, true));
			if($thread['displayorder'] != -4) {
				updatepostcredits('+', $_G['uid'], 'reply', $_G['fid']);
				if($_G['forum']['status'] == 3) {
					if($_G['forum']['closed'] > 1) {
						C::t('forum_thread')->increase($_G['forum']['closed'], $fieldarr, true);
					}
					C::t('forum_groupuser')->update_counter_for_user($_G['uid'], $_G['fid'], 0, 1);
					C::t('forum_forumfield')->update($_G['fid'], array('lastupdate' => $publishdate));
					require_once libfile('function/grouplog');
					updategroupcreditlog($_G['fid'], $_G['uid']);
				}
				
				C::t('forum_forum')->update_forum_counter($_G['fid'], 0, 1, 1);
				if($publishdate >= $_G['timestamp']){
					$lastpost = "$thread[tid]\t$thread[subject]\t$publishdate\t$author";
					C::t('forum_forum')->update($_G['fid'], array('lastpost' => $lastpost));
					if($_G['forum']['type'] == 'sub') {
						C::t('forum_forum')->update($_G['forum']['fup'], array('lastpost' => $lastpost));
					}
				}
			}
			if($updatethreaddata) {
				C::t('forum_thread')->update($_G['tid'], $updatethreaddata, false, false, 0, true);
			}
		}
	}
	
	function reply(){
		global $_G,$config;
		if(empty($this->replys)){
			return false;
		}else{
			require_once libfile('class/credit');
			require_once libfile('function/post');
			foreach($this->replys as $k => $v){
				if(!empty($config['数据库用户'])){
					$max = C::t('common_member')->max_uid();
					$randuid = rand(1, $max);
					$username_info = DB::fetch_first("SELECT uid,username FROM `".DB::table('common_member')."` where uid >= {$randuid} order by uid ASC LIMIT 1" );
					$username = $username_info['username'];
				}else{
					$username = $v['username'];
				}
				$this->logonUser($username);
				empty($v['signature']) ||	C::t('common_member_field_forum')->update($_G['uid'], array('sightml' => $v['signature']));
				$this->doreply($username , $v['message'] , $v['publishdate']);
				//$_COOKIE = null;
			}
		}
	}
	
	function check_allow_action($action = 'allowpost') {
		global $_G;
		if(isset($_G['forum'][$action]) && $_G['forum'][$action] == -1) {
			showmessage('forum_access_disallow');
		}
	}
	
	function baiduPush($titleurl,$token){
		if(trim($token)!=''){
			if(stripos($titleurl,$_SERVER['HTTP_HOST'])===false) 
				$titleurl = 'http://'.$_SERVER['HTTP_HOST'].$titleurl;
			$urls=array($titleurl);
			$baiduApi = 'http://data.zz.baidu.com/urls?site='.$_SERVER['HTTP_HOST'].'&token='.$token;
			if(function_exists('curl_init')){
				$ch = curl_init();
				$options =  array(
					CURLOPT_URL => $baiduApi,
					CURLOPT_POST => true,
					CURLOPT_TIMEOUT => 10,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_POSTFIELDS => implode("\n", $urls),
					CURLOPT_HTTPHEADER => array('Content-Type: text/plain')
				);
				curl_setopt_array($ch, $options);
				$result = curl_exec($ch);
				curl_close($ch);
			}
			else{
				$opts = array (
					'http' => array (
						'method' => 'POST',
						'timeout' => 10,
						'header'=> "Content-Type: text/plain",
						'content' => $titleurl
					)
				);
				$context = stream_context_create($opts);
				$result = @file_get_contents($baiduApi, false, $context);
			}
			$rs = json_decode($result,true);
            if($rs['success']>0) echo "Baidu Push Success<br>";
			else{
                $errormsg=$rs===false?'Network Failed':(isset($rs['message'])?$rs['message']:(isset($rs['not_same_site'])?'not_same_site':(isset($rs['not_valid'])?'url_not_valid':'Unknown')));
                echo "Baidu Push Failed, Since: {$errormsg}<br>";
            }
		}
	}

	function getPicmessage($f,$con){
		preg_match_all('/(?:<[^>]+>)*\s*<img\s+[^>]*src=[\'"](https?:\/\/[^\'"]*?)[\'"][^>]*>\s*(?:<\/[^>]*>)*/ix',$con,$rs);
		if(count($rs[0]) > 0){
			switch($f){
				case -1:/*屏蔽*/
					$con = str_replace($rs[0],'',$con);
					return $con;
				case  0:/*远程*/
					return $con;
				case  1:/*下载*/
					return $this->downloadImages($rs,$con);
					
			}
		}
		else{
			return $con;	
		}
	}	
	
	function downloadImages($rs,$con){
		if(!is_array($rs) || count($rs[0])==0 || count($rs[1])==0) return false;
		$imgPath = '/data/attachment/forum'.'/'.date("Ymd",time());
		if(!is_dir(dirname(__FILE__) . $imgPath)){	
			$this->make_dir(dirname(__FILE__) . $imgPath,0755);
		} 
		$filename = time(). mt_rand(100,999);
		$litpic = '';
		foreach($rs[1] as $key => $img){
			$img = trim($img);
			if(preg_match("#".preg_quote("http://".$_SERVER['HTTP_HOST'])."#",$img)) continue;
			$imgData = $this->curl_get_contents($img);
			if($imgData['ext']){
				$ext='.'.$imgData['ext'];
			}
			else{
				$start = strrpos($img, '.');
				$ext = substr($img,$start,strrpos($img, '?')-$start);
				if(!preg_match('/jpg|jpeg|gif|png|bmp|webp/',$ext)) $ext = '.jpg';
			}
			$imgfile = dirname(__FILE__) . $imgPath . '/' . $filename . '_' . $key . $ext;
			
			@file_put_contents($imgfile,$imgData['data']);
			$imageSize = @getimagesize($imgfile);
			if($imageSize[0]<100 || $imageSize[1]<100){
				@unlink($imgfile);
				$con = str_replace($rs[0][$key],'',$con);
				continue;
			}
			$imgUrl = "http://".$_SERVER['HTTP_HOST'] . $imgPath . '/' . $filename . '_' . $key . $ext;
			$con = str_replace($img,$imgUrl,$con);
		}
		return $con;
	}	
	
	function curl_get_contents($url, $timeout = 5){	
		if (function_exists('curl_init')){
			$ua='Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36';
			$request_header=array(
				'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
				'Accept-Language:zh-CN,zh;q=0.9',
				'Upgrade-Insecure-Requests:1',
				'Expect:'
			);
			if (!function_exists('curl_init')){
				return false;
			}
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			
			$parseUrl = parse_url($url);
			curl_setopt($curl, CURLOPT_REFERER, $parseUrl['scheme']."://".$parseUrl['host'].'/');

			curl_setopt($curl, CURLOPT_HTTPHEADER, $request_header);
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
			curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($curl, CURLOPT_HEADER, 1);
			if (ini_get('open_basedir') == '' && ini_get('safe_mode' == 'Off')) {
				curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			}
			curl_setopt($curl, CURLOPT_USERAGENT, $ua);
			curl_setopt($curl, CURLOPT_COOKIE, '');
			curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
			curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
			$result = curl_exec($curl);

			if (curl_errno($curl) === 23 || curl_errno($curl) === 61) {
				curl_setopt($curl, CURLOPT_ENCODING, 'none');
				$result = curl_exec($curl);
			}
			$this->last_curl_location = curl_getinfo($curl,CURLINFO_EFFECTIVE_URL);
			$this->httpCode=intval(curl_getinfo($curl,CURLINFO_HTTP_CODE));
			$this->last_curl_error=curl_error($curl);
			
            $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            $header = substr($result, 0, $headerSize);
			preg_match('/Content-Type:\s*image\/(jpg|jpeg|gif|png|bmp|webp)/',$header,$ext);
			$ext=isset($ext[1])?$ext[1]:false;
            $result = substr($result, $headerSize);
			curl_close($curl);
			return array('ext'=>$ext,'data'=>trim($result));
		}
		elseif(ini_get('allow_url_fopen')){
			$result=@file_get_contents($url, false, stream_context_create(array('http' => array('timeout' => $timeout)))); 
			return array('ext'=>false,'data'=>trim($result));
		}
		else return false;
	}
	function make_dir($dir, $permission = 0777){
		$dir = rtrim($dir, '/') . '/';
		if (is_dir($dir)){
			return true;
		}
		if (!$this->make_dir(dirname($dir), $permission)){
			return false;
		}
		return @mkdir($dir, $permission);
	}
	function splitPage($body){
		if($this->article['auto_sp_size'] == 0)
			return $body;
		$sp_size = $this->article['auto_sp_size'];
		$charset = $this->article['mycharset'];
		$pager='#p-a-g-e#';
		preg_match_all('/<p[^>]*?>[\s\S]*?<\/p>/i',$body,$parr);
		$body = '';
		$lastbodylen = 0;
		foreach($parr[0] as $k => $p){
			if($p == '') continue;
			$body .= $p;
			unset($parr[0][$k]);
			$bodylen = $this->cjk_strlen(strip_tags($body),$charset);
			$restlen = $this->cjk_strlen(strip_tags(join('',$parr[0])),$charset);
			if($bodylen > $lastbodylen+$sp_size && $restlen > $sp_size/3){
				$body .= $pager;
				$lastbodylen = $bodylen;
			}
		}
		return $body;
	}
	function cjk_strlen($string, $charset){
		if (function_exists('mb_strlen')){
			return mb_strlen($string, $charset);
		}
		elseif (function_exists('iconv')){
			return iconv_strlen($string, $charset);
		}
		else{
			return strlen($string);
		}
	}
	public function add_tag($tags, $itemid, $idtype = 'tid', $returnarray = 0) {
		if($tags == '' || !in_array($idtype, array('', 'tid', 'blogid', 'uid'))) {
			return;
		}

		$tags = str_replace(array(chr(0xa3).chr(0xac), chr(0xa1).chr(0x41), chr(0xef).chr(0xbc).chr(0x8c)), ',', censor($tags));
		if(strexists($tags, ',')) {
			$tagarray = array_unique(explode(',', $tags));
		} else {
			$langcore = lang('core');
			$tags = str_replace($langcore['fullblankspace'], ' ', $tags);
			$tagarray = array_unique(explode(' ', $tags));
		}

		$tagcount = 0;
		foreach($tagarray as $tagname) {
			$tagname = trim($tagname);
			if(preg_match('/^([\x7f-\xff_-]|\w|\s){3,30}$/', $tagname)) {
				$status = $idtype != 'uid' ? 0 : 3;
				$result = C::t('common_tag')->get_bytagname($tagname, $idtype);
				if($result['tagid']) {
					if($result['status'] == $status) {
						$tagid = $result['tagid'];
					}
				} else {
					$tagid = C::t('common_tag')->insert($tagname,$status);
				}

				if($tagid) {
					if($itemid) {
						C::t('common_tagitem')->replace($tagid,$itemid,$idtype);
					}
					$tagcount++;
					if(!$returnarray) {
						$return .= $tagid.','.$tagname."\t";
					} else {
						$return[$tagid] = $tagname;
					}

				}
				if($tagcount > 4) {
					unset($tagarray);
					break;
				}
			}
		}
		return $return;
	}
}
?>
